<?php

namespace AppBundle\EventListener;

use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\FOSUserEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @author Konstantin Grachev <me@grachevko.ru>
 */
final class ApiRegistrationListener implements EventSubscriberInterface
{
    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            FOSUserEvents::REGISTRATION_SUCCESS => 'onRegistrationSuccess',
            FOSUserEvents::REGISTRATION_FAILURE => 'onRegistrationFailure',
        ];
    }

    public function onRegistrationSuccess(FormEvent $event)
    {
        $request = $event->getRequest();
        if ('api_registration' !== $request->attributes->get('_route')) {
            return;
        }

        $event->setResponse(new JsonResponse('Registration Success'));
    }

    public function onRegistrationFailure(FormEvent $event)
    {
        $request = $event->getRequest();
        if ('api_registration' !== $request->attributes->get('_route')) {
            return;
        }

        $form = $event->getForm();

        $event->setResponse(new JsonResponse((string) $form->getErrors(true), JsonResponse::HTTP_BAD_REQUEST));
    }
}
