<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @author Konstantin Grachev <me@grachevko.ru>
 */
final class RegistrationFormType extends AbstractType
{
    /**
     * @var string
     */
    private $class;

    /**
     * @param string $class
     */
    public function __construct(string $class)
    {
        $this->class = $class;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', Type\EmailType::class, [
                'label' => 'form.email',
                'translation_domain' => 'FOSUserBundle',
            ])
            ->add('username', Type\TextType::class, [
                'label' => 'form.username',
                'translation_domain' => 'FOSUserBundle',
            ])
            ->add('password', Type\PasswordType::class, [
                'translation_domain' => 'FOSUserBundle',
                'label' => 'form.password',
                'invalid_message' => 'fos_user.password.mismatch',
                'property_path' => 'plainPassword',
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => $this->class,
            'csrf_protection' => false,
            'csrf_token_id' => 'registration',
        ]);
    }
}
