<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/api")
 *
 * @author Konstantin Grachev <me@grachevko.ru>
 */
class ApiDefaultController extends Controller
{
    /**
     * @Route("/", name="api_homepage")
     */
    public function indexAction()
    {
        // replace this example code with whatever you need
        return new JsonResponse(__CLASS__);
    }
}
