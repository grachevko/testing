<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

/**
 * @author Konstantin Grachev <me@grachevko.ru>
 */
class AuthFormControllerTest extends WebTestCase
{
    /**
     * @var Client
     */
    private $client;

    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function testRegistration()
    {
        $this->registration(true, 'Test user', 'test@example.com', 'Pa$$w0rd');
        $this->registration(false, 'Test user', 'test@example', 'Pa$$w0rd');
    }

    protected function registration(bool $success, string $name, string $email, string $password)
    {
        $buttonCrawlerNode = $this->client->request('GET', '/register/')->selectButton('_submit');
        $form = $buttonCrawlerNode->form();

        $this->client->submit($form, [
            'username' => $name,
            'email' => $email,
            'password' => $password,
        ]);

        $user = $this->client->getContainer()->get('doctrine.orm.entity_manager')->getConnection()->executeQuery('SELECT id FROM user WHERE email = :email', [
            'email' => $email,
        ])->fetch();

        if ($success) {
            static::assertNotEmpty($user);
        } else {
            static::assertEmpty($user);
        }
    }

    public function testLogin()
    {
        $this->login(true, 'Test user', 'Pa$$w0rd');
        $this->login(false, 'Test user', 'Pa$$w0rd___');
    }

    protected function login(bool $success, string $username, string $password)
    {
        $buttonCrawlerNode = $this->client->request('GET', '/login')->selectButton('_submit');
        $form = $buttonCrawlerNode->form();

        $this->client->submit($form, [
            '_username' => $username,
            '_password' => $password,
        ]);

        $token = $this->client->getContainer()->get('security.token_storage')->getToken();

        if ($success) {
            static::assertInstanceOf(UsernamePasswordToken::class, $token);
        } else {
            static::assertNull($token);
        }
    }
}
