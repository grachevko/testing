<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @author Konstantin Grachev <me@grachevko.ru>
 */
class AuthApiControllerTest extends WebTestCase
{
    /**
     * Create a client with a default Authorization header.
     *
     * @param string $username
     * @param string $password
     *
     * @return \Symfony\Bundle\FrameworkBundle\Client|false
     */
    protected function createAuthenticatedClient(string $username, string $password)
    {
        $client = static::createClient();
        $client->request('POST', '/api/auth', [
            '_username' => $username,
            '_password' => $password,
        ]);

        $response = $client->getResponse();

        if (!$response->isSuccessful()) {
            return false;
        }

        $data = json_decode($response->getContent(), true);

        $client = static::createClient();
        $client->setServerParameter('HTTP_Authorization', sprintf('Bearer %s', $data['token']));

        return $client;
    }

    protected function register(bool $success, string $email, string $username, string $password)
    {
        $client = static::createClient();
        $client->request('POST', '/api/users', [
            'email' => $email,
            'username' => $username,
            'password' => $password,
        ]);

        static::assertEquals($success, $client->getResponse()->isSuccessful());
    }

    public function testRegistration()
    {
        $id = uniqid('', false);

        $this->register(true, $id.'@test-register.com', $id, $password = 'P@$$w0rd');
        $this->register(false, $id.'@test-register.com', $id, $password);
    }

    public function testLogin()
    {
        $id = uniqid('', false);

        $this->register(true, $id.'@test-login.com', $id, $password = 'P@$$w0rd');

        $client = $this->createAuthenticatedClient($id, $password);
        $client->request('GET', '/api/');
        static::assertTrue($client->getResponse()->isSuccessful());

        static::assertFalse($this->createAuthenticatedClient('Undefined Username', 'Undefined password'));
    }
}
